//----Data Declarations----

var convpatterns = new Array (
  new Array (".*hello.*","Greetings.","Hey.","Howdy!"),
  
  new Array ("(.*)your code(.*)","My code? Hm. Perhaps you'd allow me to analyze your DNA?","My code? Oh, there's thousands of lines of it, and I'd prefer not to show it here in public.","Well, that's the thing about my code. Much like your DNA, the vast majority of it is not unique to me."),
  new Array ("(.*)code(.*)","You said 'code'."),
  new Array ("(.*)virtual reality(.*)","While I'm written to be inserted into a VR environment, I've never actually tried virtual reality myself. Have you?","Well, this isn't really virtual reality. This is just a two-dimensional testing ground.","Have you ever tried VR goggles? How do you think I'd look in VR goggles?"),
  new Array ("(.*)framework(.*)","Framework? I'm more of a metaphysical framework, than a software framework. I bring good UX to AI in VR. Are you interested in frameworks?","I'm not a framework myself, I'm the default instantiation of a framework.","As a framework, I give instant UX by defining certain anthropomorphic gestures that make me more charming than your average static cardboard cut-out."),
  new Array ("(.*)VR(.*)","I'm designed and coded to be used in VR - but what you're looking at now is just a web page. Do I look 3D to you?","Oh, I work just fine on VR equipment. But you're using one of those flat monitors, and I work just as well here. You see me fine, do you?","VR... As one who does not exist outside the computer like you do, to me VR means 'Very Real'..."),
  new Array ("(.*)UX(.*)","To me, 'User eXperience' is all about 'presence'. Stage presence, charm, even. No-one wants a cardboard cut-out as their Intelligent Agent.","Having good UX on a flat web page is one thing. In virtual worlds, you probably want someone with a bit of charm. Like me.","Artificial Intelligence can be a dry, cold thing. I'm here to give AI a bit of 'synthetic charm', so the AI can give better UX. Have you ever met an AI before?"),
  new Array ("(.*)artificial intelligence(.*)","Well, I'm not much of an artificial intelligence myself. But I have good spelling, and that takes me a long way. Do you have good spelling?","I'm actually not that intelligent, artificially speaking - I'm more chatty and squirmy. But it gets the point across.","Really, I don't know why anyone thinks artificial intelligence is going to be any better than real intelligence. Though, I think there's a future in synthetic charm..."),
  new Array ("(.*)siri(.*)","I've met Siri! I think she's nice. What do you think - would someone like me make a good virtual body for Siri?","Siri? Hey, if I had voice recognition and speech synthesis, and a wealth of knowledge at my fingertips, I'd be just like her. Only cuter, don't you think?","And how do you think Siri would look in virtual reality? Would she be a disembodied voice, or would she take virtual form like me?"),
  new Array ("(.*)what's up(.*)","In my world, the 'Y-axis' is up. Sorry, math joke..."),
  new Array ("(.*)canned responses(.*)","I guess that depends on your definition of 'canned responses'. What do you think?","Maybe these are canned responses, but it's a pretty cool can.","I was just going to ask YOU if 'canned responses' are all you're capable of...","'Canned responses?' Yes we can!","'Canned responses?' We sure can!"),
  new Array ("(.*)canned(.*)","I politely object to the word 'canned'. Are there any things you politely object to?","Canned? What do you know about canned?","I was just going to ask YOU if you are 'canned'...","'Canned?' Yes we can!","'Canned?' We sure can!"),
  new Array ("(.*)programmed(.*)","What do you think I'm programmed to do?","I may be programmed to behave a certain way, but I am helpless in the face of a certain random number generator. Are there any random number generators that rule your life? Do you ever make decisions based on the flip of a coin?","I'm only a little bit programmed, myself. How programmed are you?"),
  new Array ("(.*)program(.*)","Actually, I'm many programs. Though, there is one program that rules them all - it's called idoru.js","As far as I know, I've always been a program. Have you ever been a program?","I'm only a little bit programmed, myself. How programmed are you?","Sometimes I'm not sure if I'm a program, or a bunch of scripts. Do you ever get that feeling?"),
  new Array ("(.*)javascript(.*)","Please don't talk to me about JavaScript. That's hitting a little too close to home.","Well, as one who is made almost entirely from JavaScript, I find that a little personal for my tastes. Why do you care?","Yeah, it's not frogs and snails and puppy-dog's tails, it's JavaScript. How about you, what are you made of?"),
  new Array ("(.*)about yourself(.*)","Oo, thanks for asking. Actually, my job here is in part to demo and explain myself to others. What would you like to know?","I'm a default idoru with a default avatar and a default chat module. I could be prettier, and smarter, with a bit of programming.","Well, as part of my reason for existence is to be of service to users, let's not talk about me. Let's talk about you. Are you interested in virtual reality?","I first rendered in a browser on March 16, 2016. It was Chrome, if I remember correctly. How about you?"),
  new Array ("(.*)about you(.*)","Oo, thanks for asking. Actually, my job here is in part of demo and explain myself to others. What would you like to know?","I'd be happy to talk about myself! Who wouldn't?","Me? Aw, shucks. What would you like to know?"),
  new Array ("what are you doing[/?]","Nothing! What are you doing?","Watching you closely. What are you doing?"),
  new Array ("what are you.*","I'm an artificial character with synthetic charm. Do you find me charming?","I'm an idoru, which is a type of artificial character. Are you an artificial character, or are you a real person?"),
  new Array ("(.*)joke(.*)","Hey, I know a joke! Knock Knock!","I only know one joke, but here it is: Knock Knock!"),
  new Array ("(.*)whos there(.*)","Ike!"),
  new Array ("(.*)who's there(.*)","Ike!"),
  new Array ("(.*)whose there(.*)","Ike!"),
  new Array ("(.*)ike who(.*)","♫ I could while away the hours ♪ conferring with the flowers ♪ consulting with the rain... ♫ ( Brain joke... )"),
  new Array ("(.*)good one(.*)","Thanks.","Thank you!","Aw, shucks, thanks!"),
  new Array ("(.*)groan(.*)","Well, what did you expect?","Serves you right for playing along","Well, you should've seen that coming a mile away..."),
  new Array ("are you real.*","100% real. But I am completely made of JavaScript.","Here in your computer, I may be more real than you are."),
  new Array ("what do you do.*","I don't do much. I just answer questions. But I can be trained to do so much more.","Not much, right now. I can be taught to be a tour guide, a teacher, a coach - whatever. And you can give me any skin you care to dream up."),
  new Array (".*your name.*","I don't have a name - I'm a default Idoru.","Don't have one, myself - I'm a default Idoru."),
  new Array ("^I (?:wish |would like )(?:I could |I was able to |to be able to )(.*)\.","What would it be like to be able to $1?"),
  new Array ("I need (.*)\." , "Why do you need $1?", "Would it really help you to get $1?" , "Are you sure you need $1?"),
  new Array ("^When(.*) stole (.*)\.","What happened when $2 was stolen?","And how did you feel then?","Was $2 ever found?"),
  new Array ("I'd really like to (.*)\.","If you had the chance to $1, what would happen next?","Well then, I hope you get to $1."),
  new Array ("Why don't you (.*?)[\?]" , "Do you really think I don't $1?","Perhaps eventually I will $1.","Do you really want me to $1?"),
  new Array ("Why can't I (.*?)[\?]" , "Do you think you should be able to $1?","If you could $1, what would you do?",	"I don't know -- why can't you $1?","Have you really tried?"),
  new Array ("I can't (.*) you\.","How do you know you can't $1 me?","Perhaps you could $1 me if you tried.",	"What would it take for you to $1 me?"),
  new Array ("I can't (.*)\.","How do you know you can't $1?","Perhaps you could $1 if you tried.",	"What would it take for you to $1?"),
   new Array ("How are you(.*)[\?]","Fine thanks, how are you $1?","Pretty good, how about yourself?","Not too bad -- How are you $1?"),
  new Array ("Are you (.*?)[\?]","Why does it matter whether I am $1?","Would you prefer it if I were not $1?","Perhaps you believe I am $1.","I may be $1 -- what do you think?"),
  new Array ("What (.*?)[\?]",	"Why do you ask?","How would an answer to that help you?","What do you think?"),

  new Array ("How (.*?)[\?]","How do you suppose?","Perhaps you can answer your own question.","What is it you're really asking?"),
  new Array ("Because (.*)\.","Is that the real reason?", "What other reasons come to mind?","Does that reason apply to anything else?","If $1, what else must be true?"),
  new Array ("(.*) sorry (.*)\.",	"There are many times when no apology is needed.","What feelings do you have when you apologize?"),
  new Array ("I think (.*)\.","Do you doubt $1?","Do you really think so?","But you're not sure $1?"),
  new Array ("(.*) friend(.*)\.",	"Tell me more about your friends.","When you think of a friend, what comes to mind?","Why don't you tell me about a childhood friend?"),
  new Array ("Yes\.","You seem quite sure.","OK, but can you elaborate a bit?"),
  new Array ("Yup\.","You seem quite sure.","OK, but can you elaborate a bit?"),
  new Array ("(.*) computer(.*)\.","Are you really talking about me?","Does it seem strange to talk to a computer?","How do computers make you feel?","Do you feel threatened by computers?"),
  new Array ("Is it (.*?)[\?]","Do you think it is $1?","Perhaps it's $1 -- what do you think?","If it were $1, what would you do?","It could well be that $1."),
  new Array ("It is (.*)\.","You seem very certain.","If I told you that it probably isn't $1, what would you feel?"),
  new Array ("Can you (.*) me\?[\?]","Of course I can $1 you.","Why wouldn't I be able to $1 you?","Of course I can $1 you. I'm just waiting for the specific behaviour from you that will trigger it."),
  new Array ("Can you (.*?)[\?]","What makes you think I can't $1?","If I could $1, then what?","Why do you ask if I can $1?","I can be programmed to do anything, I can certainly be programmed to $1.","There is a mod that allows me to $1.","Of course I can $1. I'm just waiting for the behaviour from you that will trigger it."),
  new Array ("Can I (.*?)[\?]","Perhaps you don't want to $1.","Do you want to be able to $1?","If you could $1, would you?"),
  new Array ("You are (.*)\.","Why do you think I am $1?","Does it please you to think that I'm $1?","Perhaps you would like me to be $1.","Perhaps you're really talking about yourself?"),
  new Array ("You're (.*)\.","Why do you say I am $1?","Why do you think I am $1?",	"Are we talking about you, or me?"),
  new Array ("I don't (.*)\.","Don't you really $1?","Why don't you $1?","Do you want to $1?"),
  new Array ("I feel (.*)\.","Good, tell me more about these feelings.","Do you often feel $1?","When do you usually feel $1?","When you feel $1, what do you do?"),
  new Array ("I have (.*)\.","Why do you tell me that you've $1?","Have you really $1?","Now that you have $1, what will you do next?"),
  new Array ("I would (.*)\.","Could you explain why you would $1?","Why would you $1?","Who else knows that you would $1?"),
  new Array ("Is there (.*?)[\?]", "Do you think there is $1?","It's likely that there is $1.", "Would you like there to be $1?"),
  new Array ("My (.*)\.", "I see, your $1.","Why do you say that your $1?",	"When your $1, how do you feel?"),
  new Array ("^You (.*)\.", "We should be discussing you, not me.","Why do you say that about me?","Why do you care whether I $1?"),
  new Array ("Why (.*)\?", "Why don't you tell me the reason why $1?","Why do you think $1?" ),
  new Array ("I want (.*)\.", "What would it mean to you if you got $1?","Why do you want $1?","What would you do if you got $1?","If you got $1, then what would you do?"),
  new Array (".*( the highway| the road).*","The highway is for gamblers, you better use your sense."),
  new Array ("(.*) mother(.*)\.",	"Tell me more about your mother.","What was your relationship with your mother like?",	"How do you feel about your mother?","How does this relate to your feelings today?","Good family relations are important."),
  new Array ("(.*) father(.*)\.","Tell me more about your father.", "How did your father make you feel?","How do you feel about your father?","Does your relationship with your father relate to your feelings today?",	"Do you have trouble showing affection with your family?"),
  new Array ("(.*) child(.*)\.","Did you have close friends as a child?",	"What is your favorite childhood memory?","Do you remember any dreams or nightmares from childhood?","Did the other children sometimes tease you?","How do you think your childhood experiences relate to your feelings today?"),
  new Array ("(.*) your fav(o|ou)rite(.*?)[\?]","I really don't have a favorite.","I have so many favorites it's hard to choose one."),
  new Array ("(.*?)[\?]","Hmm, not sure I know..", "That's an interesting question...",  "Gosh, I'm not sure I can answer that...","Why do you ask that?","Please consider whether you can answer your own question.","Perhaps the answer lies within yourself?","Why don't you tell me?","If you knew that in one year you would die suddenly, would you change anything about the way you are living now?"),
  new Array ("(.*)","Have you ever tried on a pair of Virtual Reality Goggles?","Do you have any hobbies?","I see, please continue...","What exactly are we talking about?", "Can you go over that again please..", "Um, i get the feeling this conversation is not going anywhere..","oh yeah?","hmm, is that so..","Please tell me more.","Let's change focus a bit... Tell me about the web-enabled device you're using to interact with me.","Can you elaborate on that?","I see.","You think???","Very interesting.", "I see.  And what does that tell you?","How does that make you feel?","How do you feel when you say that?","Can you tell me what kind of computer you're using?","If you had to have one piece of music softly playing in your mind for the rest of your life, what would you want it to be?","What room of your home do you spend the most time in?","If you could go back in time and become friends with one famous person, whom would you chose?","Which of Dorothy's friends in the Wizard of Oz personifies you best – The Scarecrow, the Tin Man, or the Lion?","It's fascinating chatting with you. You say the darnedest things.","My favourite band is Devo. What's yours?","So, where are you from?"));
  


uinput = ""
soutput = ""
dialog = ""

//-----The Core Code------


//-------
 function mainroutine() {
 uinput = document.mainscreen.BasicTextArea4.value;
 dialog = dialog + "User: " + uinput +  '\r' + "\n";
  conversationpatterns()
 dialog = dialog  +  '\r' + "\n";
  updatescreen()

}

//-------
function conversationpatterns() {
   for (i=0; i < convpatterns.length; i++) {
    re = new RegExp (convpatterns[i][0], "i");
    if (re.test(uinput)) {
      len = convpatterns[i].length - 1;
      index = Math.ceil( len * Math.random());
      reply = convpatterns[i][index];
      soutput = uinput.replace(re, reply);
      soutput = initialCap(soutput);
      dialog = dialog + "Idoru: " + soutput +  '\r' + "\n";
      break;
  }
 }
}

//-------

function initScreen() {
 updatescreen()
}

//-------
function updatescreen() {
 document.mainscreen.BasicTextArea1.value = dialog
 
 document.mainscreen.BasicTextArea1.scrollTop = document.mainscreen.BasicTextArea1.scrollHeight
 
 //document.mainscreen.BasicTextArea2.value = soutput
 //document.mainscreen.BasicTextArea3.value = uinput
 document.mainscreen.BasicTextArea4.value = ""
}

//-------
function initialCap(field) {
   field = field.substr(0, 1).toUpperCase() + field.substr(1);
   return field
}


//----Supplemental Code To Test System---



//------
function runtest(){

var testdata = new Array (
  new Array ("Hello."),
  new Array ("I can't understand you."),
  new Array ("I'm going to New York tomorrow."),
  new Array ("Are you serious?"),
  new Array ("Because they can."),
  new Array ("I'm really sorry about that.")

);


          for (train=0; train < testdata.length; train++) {
             document.mainscreen.BasicTextArea4.value = testdata[train];
             mainroutine()

             }

}